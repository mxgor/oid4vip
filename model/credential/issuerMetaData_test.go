package credential

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/oauth"
)

func Test_FindOpenIdConfiguration(t *testing.T) {

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		m := oauth.OpenIdConfiguration{
			Grant_Types_Supported: []string{string("bla")},
			Jwks_Uri:              "test",
		}

		b, _ := json.Marshal(m)

		w.Write(b)
	}))

	srv2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		m := oauth.OpenIdConfiguration{
			Grant_Types_Supported: []string{string(oauth.PreAuthorizedCodeGrant)},
			Jwks_Uri:              "test2",
		}

		b, _ := json.Marshal(m)

		w.Write(b)
	}))

	metadata := IssuerMetadata{
		AuthorizationServers: []string{srv.URL, srv2.URL},
	}

	config, err := metadata.FindFittingAuthorizationServer(oauth.PreAuthorizedCodeGrant)

	if err != nil || config.Jwks_Uri != "test2" {
		t.Error()
	}

}
