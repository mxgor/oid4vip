package presentation

import (
	"encoding/json"
	"testing"
)

func TestInputDescriptorFiltering(t *testing.T) {
	var definition PresentationDefinition
	err := json.Unmarshal([]byte(testDefinition), &definition)

	if err != nil {
		t.Error()
	}

	var credentials = make(map[string]interface{}, 0)

	credentials["123"] = credential
	credentials["55"] = credential2
	credentials["666"] = credential3

	res, err := definition.Filter(credentials)

	if err != nil || res == nil {
		t.Error()
	}

	if len(res[0].Credentials) != 2 {
		t.Error()
	}

}

func TestInputDescriptorFilteringWithEmptyDefinition(t *testing.T) {
	var definition PresentationDefinition
	err := json.Unmarshal([]byte(testEmptyDefinition), &definition)

	if err != nil {
		t.Error()
	}

	var credentials = make(map[string]interface{}, 0)

	credentials["123"] = credential
	credentials["55"] = credential2
	credentials["666"] = credential3

	res, err := definition.Filter(credentials)

	if err != nil || res == nil {
		t.Error()
	}

	if len(res[0].Credentials) != 3 {
		t.Error()
	}

}

func TestMultipleInputs(t *testing.T) {
	var definition PresentationDefinition
	err := json.Unmarshal([]byte(multipleDescriptors), &definition)

	if err != nil {
		t.Error()
		return
	}

	var credentials = make(map[string]interface{}, 0)

	credentials["123"] = credential
	credentials["666"] = credential3

	res, err := definition.Filter(credentials)

	if res[0].Credentials == nil || res[1].Credentials == nil {
		t.Error()
	}

	if len(res[0].Credentials) != 1 {
		t.Error()
	}

	if len(res[0].Credentials) != 1 {
		t.Error()
	}

	_, ok := res[0].Credentials["123"]

	if !ok {
		t.Error()
	}

	_, ok = res[1].Credentials["666"]

	if !ok {
		t.Error()
	}
}

func TestFields(t *testing.T) {
	var definition PresentationDefinition
	err := json.Unmarshal([]byte(testDefinitionForFields), &definition)

	if err != nil {
		t.Error()
		return
	}

	var credentials = make(map[string]interface{}, 0)

	credentials["123"] = credential
	credentials["666"] = credential4

	res, err := definition.Filter(credentials)

	if len(res[0].Credentials) != 1 {
		t.Error()
	}

	_, ok := res[0].Credentials["666"]

	if !ok {
		t.Error()
	}

}

func TestMarshalling(t *testing.T) {
	var definition PresentationDefinition
	err := json.Unmarshal([]byte(testEmptyDefinition), &definition)

	if err != nil {
		t.Error()
	}

	var credentials = make(map[string]interface{}, 0)

	credentials["123"] = credential
	credentials["55"] = credential2
	credentials["666"] = credential3

	res, err := definition.Filter(credentials)

	if err != nil {
		t.Error()
	}

	b, err := json.Marshal(res)

	if err != nil {
		t.Error()
	}

	var j []map[string]map[string]map[string]map[string]interface{}

	err = json.Unmarshal(b, &j)

	if j[0]["credentials"]["123"]["credentialSubject"]["dob"] != "12222" {
		t.Error()
	}

}

const credential = `{
	"credentialSubject":{
		"dob":"12222"
	}
}`

const credential2 = `{
	"credentialSubject":{
		"xyz":"12222"
	}
}`

const credential3 = `{
	"credentialSubject":{
		"dateOfBirth":"12222"
	}
}`

const credential4 = `{
	"credentialSubject":{
		"dateOfBirth":"12222",
		"xyz":"111"
	}
}`

const testDefinition = `{
	  "id": "32f54163-7166-48f1-93d8-ff217bdb0653",
	  "input_descriptors": [
		{
		  "id": "wa_driver_license",
		  "name": "Washington State Business License",
		  "purpose": "We can only allow licensed Washington State business representatives into the WA Business Conference",
		  "constraints": {
			"fields": [
			  {
				"path": [
				  "$.credentialSubject.dateOfBirth",
				  "$.credentialSubject.dob",
				  "$.vc.credentialSubject.dateOfBirth",
				  "$.vc.credentialSubject.dob"
				]
			  }
			]
		  }
		}
	  ]
  }`

const testDefinitionForFields = `{
	"id": "32f54163-7166-48f1-93d8-ff217bdb0653",
	"input_descriptors": [
	  {
		"id": "wa_driver_license",
		"name": "Washington State Business License",
		"purpose": "We can only allow licensed Washington State business representatives into the WA Business Conference",
		"constraints": {
		  "fields": [
			{
			  "path": [
				"$.credentialSubject.dateOfBirth"
			  ]
			},
			{
				"path": [
				  "$.credentialSubject.xyz"
				]
			}
		  ]
		}
	  }
	]
}`

const multipleDescriptors = `{
	"id": "32f54163-7166-48f1-93d8-ff217bdb0653",
	"input_descriptors": [
	  {
		"id": "wa_driver_license",
		"name": "Washington State Business License",
		"purpose": "We can only allow licensed Washington State business representatives into the WA Business Conference",
		"constraints": {
		  "fields": [
			{
			  "path": [
				"$.credentialSubject.dob"
			  ]
			}
		  ]
		}},
		{
			"id": "personalid",
			"name": "Washington State ID card",
			"constraints": {
			  "fields": [
				{
				  "path": [
					"$.credentialSubject.dateOfBirth"
				  ]
				}
			  ]
			}
	  }
	]
}`

const testEmptyDefinition = `{
	"id": "32f54163-7166-48f1-93d8-ff217bdb0653",
	"input_descriptors": []
}`
