package types

import (
	"encoding/json"
	"errors"

	"github.com/lestrrat-go/jwx/v2/jwt"
)

type CredentialFormat string
type PresentationFormat string

const (
	SDJWT   CredentialFormat = "vc+sd-jwt"
	JWTVC   CredentialFormat = "jwt_vc"
	LDPVC   CredentialFormat = "ldp_vc"
	UNKNOWN CredentialFormat = "unknown"
)

const (
	LDPVP PresentationFormat = "ldp_vp"
)

type Credential struct {
	Format CredentialFormat
	Json   map[string]interface{}
}

func CheckFormat(credential interface{}) (*Credential, error) {

	c := Credential{
		Format: UNKNOWN,
		Json:   nil,
	}

	if credential == nil {
		return &c, errors.New("credential nil")
	}

	s, ok := credential.(string)

	if ok {
		var j map[string]interface{}

		err := json.Unmarshal([]byte(s), &j)

		if err == nil {
			c.Format = LDPVC
			c.Json = j
			return &c, nil
		}

		tok, err := jwt.ParseInsecure([]byte(s))

		if err != nil {
			return &c, err
		}

		c.Format = JWTVC
		c.Json = tok.PrivateClaims()
		return &c, nil
	}

	return &c, errors.ErrUnsupported
}
